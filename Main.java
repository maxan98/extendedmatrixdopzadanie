import kek.maxan98.core.HardMatrix;
import kek.maxan98.core.MyMatrix;
import kek.maxan98.core.MySqMatrix;
import kek.maxan98.core.exc.MatrixMulException;
import kek.maxan98.core.exc.MatrixSumException;
import kek.maxan98.core.exc.MyException;

/**
 * Created by MaksSklyarov on 17.02.17.
 */
public class Main {
    public static void main(String[] args) {
        long x = System.currentTimeMillis();
    MyMatrix m = new MyMatrix(5,3);
    m.init(3);
    MyMatrix m1 = new MyMatrix(3,4);
    m1.init(4);
    MyMatrix m2 = new MyMatrix(45,5);
    m.setData(2,2,1000);
    try{
    m2 = m.mul(m1);}
    catch(MyException ex){
        int n = ex.getErrorCode();
        System.out.println(ex.getMessage() + " " + ex.getErrorCode());
    }
        m2.print();
        MySqMatrix t2 = new MySqMatrix(5);
        t2.zero();

        MySqMatrix t3 = new MySqMatrix(5);
        t3.init(3);
        MySqMatrix t4 = new MySqMatrix(5);
        t4.init(5);
        try {
            t2 = t3.mul(t4);
        }catch(MyException ex){
            System.out.println(ex.getMessage() + " " + ex.getErrorCode());
        }

        t2.print();

        if(t2.equals(t2)){
            System.out.println("Yea");
        }
        HardMatrix hm = new HardMatrix(6, 6);
        HardMatrix hm2 = new HardMatrix(6, 6);
        MyMatrix hm3 = new MyMatrix(6,6);
        hm2.init(6);
        hm.init(4);
//            hm.normalize();
        hm.setData(1,4,5);
        hm.setData(5,1,3);
        hm.setData(5,5,7);

            hm3 = hm.sum(hm2);


        System.out.println(hm2);
        System.out.println(hm);
        System.out.println(hm3);
        long x1 = System.currentTimeMillis();
        long x2 = x1-x;
        System.out.println("All done! It took me " + x2 + "ms to complete.");

    }

}
